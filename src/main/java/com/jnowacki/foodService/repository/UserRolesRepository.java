package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by jnowacki on 8/20/16.
 */

@Repository
public interface UserRolesRepository extends CrudRepository<UserRole, Long>
{
    UserRole getByRoleName(String roleName);
}
