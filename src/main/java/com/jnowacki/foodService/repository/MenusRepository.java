package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.Menu;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by jnowacki on 7/31/16.
 */
public interface MenusRepository extends CrudRepository<Menu, Long>
{
}
