package com.jnowacki.foodService.repository;

import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.entity.OrderItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by jnowacki on 7/31/16.
 */
public interface OrderItemsRepository extends CrudRepository<OrderItem, Long>
{
    List<OrderItem> findByOrder(Order order);
}
