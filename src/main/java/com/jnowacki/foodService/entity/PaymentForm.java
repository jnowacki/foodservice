package com.jnowacki.foodService.entity;

import javax.persistence.*;

/**
 * Created by jnowacki on 7/31/16.
 */

@Entity
@Table(name = "payment_forms")
public class PaymentForm
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    public PaymentForm()
    {
    }

    public PaymentForm(String name)
    {
        this.name = name;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "PaymentForm{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
