package com.jnowacki.foodService.entity;

import com.jnowacki.foodService.util.CalcUtils;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by jnowacki on 7/31/16.
 */

@Entity
@Table(name = "order_items")
public class OrderItem
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    @ManyToOne
    @JoinColumn(name = "dish_id", nullable = false)
    private Dish dish;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "payment_form_id", nullable = false)
    private PaymentForm paymentForm;

    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "owner", nullable = false, length = 45)
    private String owner;

    @Size(max = 250)
    @Column(name = "comment", length = 250)
    private String comment;

    @NotNull
    @Min(1)
    @Max(10)
    @Column(name = "quantity", nullable = false)
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Size(min = 1)
    @Column(name = "hash", length = 64)
    private String hash;

    @Column(name = "paid", nullable = false)
    private boolean isPaidFor;

    public OrderItem()
    {
    }

    public OrderItem(Order order, Dish dish, PaymentForm paymentForm, String owner, String comment, int quantity, User user, String hash)
    {
        this.order = order;
        this.dish = dish;
        this.paymentForm = paymentForm;
        this.owner = owner;
        this.comment = comment;
        this.quantity = quantity;
        this.user = user;
        this.hash = hash;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Order getOrder()
    {
        return order;
    }

    public void setOrder(Order order)
    {
        this.order = order;
    }

    public Dish getDish()
    {
        return dish;
    }

    public void setDish(Dish dish)
    {
        this.dish = dish;
    }

    public PaymentForm getPaymentForm()
    {
        return paymentForm;
    }

    public void setPaymentForm(PaymentForm paymentForm)
    {
        this.paymentForm = paymentForm;
    }

    public String getOwner()
    {
        return owner;
    }

    public void setOwner(String owner)
    {
        this.owner = owner;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public int getQuantity()
    {
        return quantity;
    }

    public void setQuantity(int quantity)
    {
        this.quantity = quantity;
    }

    public String getHash()
    {
        return hash;
    }

    public void setHash(String strToHash)
    {
        this.hash = CalcUtils.hashPasswordSHA256(strToHash);
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public boolean isPaidFor()
    {
        return isPaidFor;
    }

    public void setPaidFor(boolean paidFor)
    {
        isPaidFor = paidFor;
    }

    @Override
    public String toString()
    {
        return "OrderItem{" +
                "id=" + id +
                ", order=" + order +
                ", dish=" + dish +
                ", paymentForm=" + paymentForm +
                ", owner='" + owner + '\'' +
                ", comment='" + comment + '\'' +
                ", quantity=" + quantity +
                ", user=" + user +
                ", hash='" + hash + '\'' +
                ", isPaidFor=" + isPaidFor +
                '}';
    }
}
