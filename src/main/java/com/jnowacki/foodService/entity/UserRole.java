package com.jnowacki.foodService.entity;

import javax.persistence.*;

/**
 * Created by jnowacki on 8/20/16.
 */

@Entity
@Table(name = "user_roles")
public class UserRole
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "role_name", nullable = false)
    private String roleName;

    public UserRole()
    {
    }

    public UserRole(String roleName)
    {
        this.roleName = roleName;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    @Override
    public String toString()
    {
        return "UserRole{" +
                "id=" + id +
                ", role='" + roleName + '\'' +
                '}';
    }

    public String getRoleName()
    {
        return roleName;
    }

    public void setRoleName(String role)
    {
        this.roleName = role;
    }
}
