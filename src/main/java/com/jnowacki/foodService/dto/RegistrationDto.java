package com.jnowacki.foodService.dto;

import com.jnowacki.foodService.util.PasswordMatches;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Size;

/**
 * Created by jnowacki on 8/21/16.
 */

@Component
@PasswordMatches
public class RegistrationDto
{
    @Email
    @NotBlank
    private String email;

    @Size(min = 8)
    private String password;

    private String confirmedPassword;

    public RegistrationDto()
    {
    }

    public RegistrationDto(String email, String password, String confirmPassword)
    {
        this.email = email;
        this.password = password;
        this.confirmedPassword = confirmPassword;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getConfirmedPassword()
    {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword)
    {
        this.confirmedPassword = confirmedPassword;
    }

    @Override
    public String toString()
    {
        return "RegistrationDto{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", confirmedPassword='" + confirmedPassword + '\'' +
                '}';
    }
}
