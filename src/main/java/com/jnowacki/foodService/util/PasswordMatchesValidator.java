package com.jnowacki.foodService.util;

import com.jnowacki.foodService.dto.RegistrationDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by jnowacki on 8/21/16.
 */

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object>
{

    @Override
    public void initialize(PasswordMatches constraintAnnotation)
    {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context)
    {
        RegistrationDto registrationDto = (RegistrationDto) obj;
        return registrationDto.getPassword().equals(registrationDto.getConfirmedPassword());
    }
}