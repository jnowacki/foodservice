package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.Menu;
import com.jnowacki.foodService.repository.MenusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by jnowacki on 7/31/16.
 */

@Service
public class MenusService
{
    @Autowired
    private MenusRepository menusRepository;

    public List<Menu> getAll()
    {
        return (List<Menu>) menusRepository.findAll();
    }

    public Menu getEmpty()
    {
        return new Menu();
    }

    public Menu save(Menu menu)
    {
        return menusRepository.save(menu);
    }

    public Menu getById(Long id)
    {
        return menusRepository.findOne(id);
    }

}
