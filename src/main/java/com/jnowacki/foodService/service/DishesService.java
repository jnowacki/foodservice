package com.jnowacki.foodService.service;

import com.jnowacki.foodService.entity.Dish;
import com.jnowacki.foodService.entity.Menu;
import com.jnowacki.foodService.entity.OrderItem;
import com.jnowacki.foodService.repository.DishesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jnowacki on 8/5/16.
 */

@Service
public class DishesService
{
    @Autowired
    private DishesRepository dishesRepository;

    public List<Dish> getAll()
    {
        return (List<Dish>) dishesRepository.findAll();
    }

    public List<Dish> getFromMenu(Menu menu)
    {
        return dishesRepository.findByMenuId(menu);
    }

    public Dish getById(Long id)
    {
        return dishesRepository.findOne(id);
    }

    public Dish getEmpty()
    {
        return new Dish();
    }

    public void save(Dish dish)
    {
        dishesRepository.save(dish);
    }

    public HashMap<Dish, Integer> getUniqueDishes(List<OrderItem> items)
    {
        HashMap<Dish, Integer> summary = new HashMap<>();

        for(OrderItem item: items)
        {
            Dish key = item.getDish();
            int value = item.getQuantity();

            if(summary.containsKey(key))
            {
                summary.replace(key, summary.get(key) + value);
            }
            else
            {
                summary.put(key,value);
            }
        }

        return summary;
    }

    public float getDishesCost(List<OrderItem> items)
    {
        float totalCost = 0;

        for(OrderItem item: items)
        {
            totalCost += item.getQuantity() * item.getDish().getPrice();
        }

        return totalCost;
    }
}
