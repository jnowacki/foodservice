package com.jnowacki.foodService.controller;

import com.jnowacki.foodService.entity.Dish;
import com.jnowacki.foodService.service.DishesService;
import com.jnowacki.foodService.service.MenusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

/**
 * Created by jnowacki on 8/6/16.
 */

@Controller
public class DishesController
{
    @Autowired
    private DishesService dishesService;

    @Autowired
    private MenusService menusService;

    @RequestMapping(value = "/newDish", method = RequestMethod.GET)
    public String returnNewDish(@RequestParam(value = "menuId") Long menuId, Model model)
    {
        if (!model.containsAttribute("dish"))
        {
            Dish dish = dishesService.getEmpty();
            dish.setMenuId(menusService.getById(menuId));

            model.addAttribute("dish", dish);
        }

        model.addAttribute("message", "Utwórz nowe danie");

        return "createDish";
    }

    @RequestMapping(value = "/dishes/{dishId}", method = RequestMethod.GET)
    public String returnDish(@PathVariable(value = "dishId") Long dishId, Model model)
    {
        if (!model.containsAttribute("dish"))
        {
            model.addAttribute("dish", dishesService.getById(dishId));
        }

        model.addAttribute("message", "Edytuj danie: " + dishesService.getById(dishId).getName());

        return "createDish";
    }


    @RequestMapping(value = "/dishes", method = RequestMethod.POST)
    public String returnMenus(@ModelAttribute @Valid Dish dish, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {
        if (bindingResult.hasErrors())
        {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.dish", bindingResult);
            redirectAttributes.addFlashAttribute("dish", dish);

            if (dish.getId() == null)
            {
                return "redirect:newDish?menuId=" + dish.getMenuId().getId();
            }

            return "redirect:dishes/" + dish.getId();
        }

        dishesService.save(dish);

        return "redirect:menus/" + dish.getMenuId().getId();
    }
}
