package com.jnowacki.foodService.controller;

import com.jnowacki.foodService.entity.Dish;
import com.jnowacki.foodService.entity.Order;
import com.jnowacki.foodService.entity.OrderItem;
import com.jnowacki.foodService.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jnowacki on 8/10/16.
 */

@Controller
public class SummaryController
{
    @Autowired
    private MenusService menusService;

    @Autowired
    private DishesService dishesService;

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private ItemsService itemsService;

    @RequestMapping(value = "/orders/summary/{orderId}", method = RequestMethod.GET)
    public String manageOrder(@PathVariable(value = "orderId") Long orderId, Model model)
    {
        Order order = ordersService.getById(orderId);
        List<OrderItem> fromOrder = itemsService.getFromOrder(order);
        HashMap<Dish, Integer> summary = dishesService.getUniqueDishes(fromOrder);

        model.addAttribute("message", "Zarządzanie zamówieniem: " + order.getName());
        model.addAttribute("order", order);
        model.addAttribute("items", fromOrder);
        model.addAttribute("totalCost", dishesService.getDishesCost(fromOrder));
        model.addAttribute("summary", summary);

        return "summary";
    }


}
