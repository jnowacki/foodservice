package com.jnowacki.foodService.selenium.test;

import com.jnowacki.foodService.selenium.pageObjects.ActiveOrdersPage;
import com.jnowacki.foodService.selenium.pageObjects.CreateOrderPage;
import com.jnowacki.foodService.selenium.test.helper.BaseTest;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeFalse;

/**
 * Created by jnowacki on 9/11/16.
 */
public class OrderCreationPageTests extends BaseTest
{
    @Autowired
    private CreateOrderPage createOrderPage;

    @Autowired
    private ActiveOrdersPage activeOrdersPage;

    @Before
    public void setUp()
    {
        createOrderPage.goToHomePage();
    }

    @Test
    public void createNewOrderTest()
    {
        //data for creating order
        String orderName = "test order " + new Date(); //Date for unique name
        String endTime = "12/12/2100 12:00";
        String owner = "test owner";
        String comment = "comment";
        String password = "qwe";

        //given
        createOrderPage.navigate();

        //when
        createOrderPage.putOrderName(orderName);
        createOrderPage.putEndTime(endTime);
        createOrderPage.putOwner(owner);
        createOrderPage.putComment(comment);
        createOrderPage.putPassword(password);
        createOrderPage.toggleCashChecbox();
        String selectedMenu = createOrderPage.chooseRandomMenu();


        //create order "row" to compare order when menu was chosen
        String orderRow = orderName + " " + selectedMenu + " " + endTime + " " + owner + " " + comment;

        createOrderPage.clickSave();

        //then
        activeOrdersPage.navigate();

        assertTrue(activeOrdersPage.getRows().contains(orderRow));
    }

    @Test
    public void createNewOrderWithEmptyData()
    {
        //data for creating order
        String orderName = "";
        String endTime = "";
        String owner = "";

        //given
        createOrderPage.navigate();

        //when
        createOrderPage.putOrderName(orderName);
        createOrderPage.putEndTime(endTime);
        createOrderPage.putOwner(owner);
        createOrderPage.toggleCashChecbox();
        String selectedMenu = createOrderPage.chooseRandomMenu();

        createOrderPage.clickSave();

        //then
        assertTrue(createOrderPage.isNameErrorVisible());
        assertTrue(createOrderPage.isEndTimeErrorVisible());
        assertTrue(createOrderPage.isOwnerErrorVisible());
    }

    @Test
    public void createNewOrderWithMalformedDate()
    {
        //data for creating order
        String orderName = "test order";
        String endTime = "malformed end time";
        String owner = "test owner";

        //given
        createOrderPage.navigate();

        //when
        createOrderPage.putOrderName(orderName);
        createOrderPage.putEndTime(endTime);
        createOrderPage.putOwner(owner);
        createOrderPage.toggleCashChecbox();
        String selectedMenu = createOrderPage.chooseRandomMenu();

        createOrderPage.clickSave();

        //then
        assertTrue(createOrderPage.isEndTimeErrorVisible());
    }

    @Test
    public void createNewOrderWithPastDate()
    {
        //data for creating order
        String orderName = "test order";
        String endTime = "01/01/1970 00:00";
        String owner = "test owner";

        //given
        createOrderPage.navigate();

        //when
        createOrderPage.putOrderName(orderName);
        createOrderPage.putEndTime(endTime);
        createOrderPage.putOwner(owner);
        createOrderPage.toggleCashChecbox();
        String selectedMenu = createOrderPage.chooseRandomMenu();

        createOrderPage.clickSave();

        //then
        assertTrue(createOrderPage.isEndTimeErrorVisible());
    }

    @Test
    public void createNewOrderWithNoPayment()
    {
        //data for creating order
        String orderName = "test order";
        String endTime = "12/12/2100 12:00";
        String owner = "test owner";

        //given
        createOrderPage.navigate();

        //when
        createOrderPage.putOrderName(orderName);
        createOrderPage.putEndTime(endTime);
        createOrderPage.putOwner(owner);
        String selectedMenu = createOrderPage.chooseRandomMenu();

        createOrderPage.clickSave();

        //then
        assertTrue(createOrderPage.isPaymentErrorVisible());
    }

    @Test
    public void createNewOrderWithEmptyPassword()
    {
        //data for creating order
        String orderName = "test order";
        String endTime = "12/12/2100 12:00";
        String owner = "test owner";

        //given
        createOrderPage.navigate();

        //when
        createOrderPage.putOrderName(orderName);
        createOrderPage.putEndTime(endTime);
        createOrderPage.putOwner(owner);
        createOrderPage.toggleCashChecbox();
        String selectedMenu = createOrderPage.chooseRandomMenu();

        createOrderPage.clickSave();

        //then
        assertTrue(createOrderPage.isPasswordErrorVisible());
    }
}
