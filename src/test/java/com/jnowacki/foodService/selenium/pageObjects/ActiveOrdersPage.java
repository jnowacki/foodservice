package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class ActiveOrdersPage extends BasicPage
{
    @FindBy(id = "order-table")
    private WebElement ordersTable;

    @Autowired
    private ItemListPage itemListPage;

    @Autowired
    public ActiveOrdersPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public List<String> getRows()
    {
        return getTableRowsTexts(ordersTable);
    }

    public ItemListPage enterFirstOrder()
    {
        ordersTable.findElement(By.cssSelector("tr[style='cursor: pointer']")).click();

        return itemListPage;
    }

    public void navigate()
    {
        goToPage("orders?active=true");
    }
}
