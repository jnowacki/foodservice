package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jnowacki on 9/12/16.
 */

@Component
public class CreateItemPage extends BasicPage
{
    @FindBy(id = "owner")
    private WebElement ownerField;

    @FindBy(id = "password")
    private WebElement passwordField;

    @FindBy(id = "saveButton")
    private WebElement saveButton;

    @Autowired
    private ItemListPage itemListPage;

    @Autowired
    public CreateItemPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public void putOwner(String owner)
    {
        ownerField.sendKeys(owner);
    }

    public void putPassword(String password)
    {
        passwordField.sendKeys(password);
    }

    public ItemListPage clickSave()
    {
        saveButton.click();

        return itemListPage;
    }
}
