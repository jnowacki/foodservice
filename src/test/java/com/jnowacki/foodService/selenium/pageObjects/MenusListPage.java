package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class MenusListPage extends BasicPage
{
    @FindBy(id = "menu-list")
    private WebElement menusTable;

    @Autowired
    public MenusListPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public List<String> getRows()
    {
        return getTableRowsTexts(menusTable);
    }

    public void navigate()
    {
        goToPage("menus");
    }
}
