package com.jnowacki.foodService.selenium.pageObjects.helper;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jnowacki on 9/1/16.
 */

public abstract class BasicPage
{
    private String pageAddress = "http://localhost:8080/";

    @Autowired
    public LeftMenu leftMenu;

    @Autowired
    public TopBar topBar;

    @Autowired
    private WebDriver webDriver;

    public void goToHomePage()
    {
        webDriver.get(pageAddress);
    }

    public void close()
    {
        webDriver.quit();
    }


    protected void goToPage(String subpage)
    {
        webDriver.get(pageAddress + subpage);
    }

    protected boolean isOnPage(String url)
    {
        return webDriver.getCurrentUrl().contains(url);
    }

    protected List<String> getTableRowsTexts(WebElement table)
    {
        List <String> rowsTexts = new ArrayList<>();

        for (WebElement row : table.findElements(By.tagName("tr")))
        {
            rowsTexts.add(row.getText());
        }

        return rowsTexts;
    }
}
