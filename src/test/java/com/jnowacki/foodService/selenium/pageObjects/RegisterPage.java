package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.springframework.stereotype.Component;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class RegisterPage extends BasicPage
{
    //TODO: Only used to be returned from other page objects, implement content
}
