package com.jnowacki.foodService.selenium.pageObjects;

import com.jnowacki.foodService.selenium.pageObjects.helper.BasicPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jnowacki on 9/1/16.
 */

@Component
public class CreateMenuPage extends BasicPage
{
    @FindBy(id = "restaurantName")
    private WebElement nameField;

    @FindBy(id = "sourceUrl")
    private WebElement sourceUrlField;

    @FindBy(id = "comment")
    private WebElement commentField;

    @FindBy(id = "saveButton")
    private WebElement saveButton;

    @FindBy(id = "nameError")
    private WebElement nameError;

    @FindBy(id = "urlError")
    private WebElement urlError;

    @Autowired
    private EditMenuPage editMenuPage;

    @Autowired
    public CreateMenuPage(WebDriver webDriver)
    {
        PageFactory.initElements(webDriver, this);
    }

    public void putRestaurantName(String name)
    {
        nameField.sendKeys(name);
    }

    public void putSourceUrl(String sourceUrl)
    {
        sourceUrlField.sendKeys(sourceUrl);
    }

    public void putComment(String comment)
    {
        commentField.sendKeys(comment);
    }

    public EditMenuPage clickSave()
    {
        saveButton.click();

        return editMenuPage;
    }

    public boolean isNameErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            nameError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public boolean isSourceUrlErrorVisible()
    {
        boolean isErrorVisible = true;

        try
        {
            urlError.isDisplayed();
        }
        catch (Exception e)
        {
            isErrorVisible = false;
        }

        return isOnPage() && isErrorVisible;
    }

    public void navigate()
    {
        goToPage("newMenu");
    }

    public boolean isOnPage()
    {
        return isOnPage("/newMenu");
    }
}
